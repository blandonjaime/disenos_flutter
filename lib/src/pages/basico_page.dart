import 'package:flutter/material.dart';

class BasicoPage extends StatelessWidget {

  final estiloTitulo = TextStyle( 
    fontSize: 20.0,
    fontWeight: FontWeight.bold);
  
  final estiloSubTitulo = TextStyle( 
    fontSize: 18.0,
    color: Colors.grey);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
              _crearImagen(),
              _crearTitulo(),
              _crearAcciones(),
              _crearTexto(),
              _crearTexto(),
              _crearTexto(),
              _crearTexto(),
              _crearTexto(),
          ],
        ),
      )
    );
  }

  Widget _crearTexto() {
    return SafeArea(
          child: Container(
        padding: EdgeInsets.symmetric(horizontal:40.0,vertical:20.0),
        child: Text(
          'Eiusmod ipsum minim veniam tempor magna sunt aliquip enim minim ad laboris quis. Esse nisi enim nulla magna laborum ipsum do ex fugiat esse elit non velit ea. Laborum consequat nostrud occaecat nulla id ea laborum. Sit commodo id sunt aliqua nulla cupidatat. In cillum ex dolore sit quis veniam commodo minim eu elit ipsum consequat. Ipsum sit officia est Lorem dolor enim sunt nostrud.',
          textAlign: TextAlign.justify,
        ),
      ),
    );
  }

  Widget _crearAcciones() {

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

      children: <Widget>[
        _crearColumna('CALL', Icons.call),
        _crearColumna('ROUTE', Icons.near_me),
        _crearColumna('SHARE', Icons.share),
      ],
    );

  }

  Widget _crearColumna(String text, IconData icono){
    return Column(
          children: <Widget>[
         Icon(icono,color: Colors.blue,size: 40.0,),
         SizedBox(height: 5.0),
         Text(
           text,
           style: TextStyle(
             fontSize: 15.0,
             color: Colors.blue
           )
         )
       ],
      );
  }

  Widget _crearImagen() {
    return Container(
      width: double.infinity,
      child: FadeInImage(
                image: NetworkImage('https://i.ytimg.com/vi/c7oV1T2j5mc/maxresdefault.jpg'),
                placeholder: AssetImage('assets/img/loading.gif'),
                height: 250.0,
                fit: BoxFit.cover,
              ),
    );
  }

  Widget _crearTitulo() {
    return SafeArea(
          child: Container(
                padding: EdgeInsets.symmetric(horizontal: 30.0,vertical: 20.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Lago con un puente',style: estiloTitulo,),
                          SizedBox(height: 7.0,),
                          Text('Un lago en Alemania',style: estiloSubTitulo),
                        ],
                      ),
                    ),

                    Icon( Icons.star, color: Colors.red, size: 30.0 ),
                    Text('41',style: TextStyle(fontSize: 20.0))
                  ],
                ),
              ),
    );
  }

}